# OpenML dataset: Meta_Album_TEX_ALOT_Micro

https://www.openml.org/d/44274

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Textures-ALOT Dataset (Micro)**
***
Textures ALOT dataset (https://aloi.science.uva.nl/public_alot/) consists of 27 500 images from 250 categories. The images in the dataset are captured in controlled environment by the creators of the dataset. The images have different viewing angle, illumination angle, and illumination color for each material of texture. A preprocessed version of Textures-ALOT is used in the Meta-Album meta-dataset. The images are first cropped into square images and then resized to 128x128 with anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/TEX_ALOT.png)

**Meta Album ID**: MNF.TEX_ALOT  
**Meta Album URL**: [https://meta-album.github.io/datasets/TEX_ALOT.html](https://meta-album.github.io/datasets/TEX_ALOT.html)  
**Domain ID**: MNF  
**Domain Name**: Manufacturing  
**Dataset ID**: TEX_ALOT  
**Dataset Name**: Textures-ALOT  
**Short Description**: Textures dataset from Amsterdam Library of Textures (ALOT)  
**\# Classes**: 20  
**\# Images**: 800  
**Keywords**: textures, manufacturing  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Open for research, cite paper to use dataset  
**License (Meta-Album data release)**: CC BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: Amsterdam Library of Textures (ALOT), University of Amsterdam, Netherlands  
**Source URL**: https://aloi.science.uva.nl/public_alot/  
  
**Original Author**: Gertjan Burghouts, Jan-Mark Geusebroek  
**Original contact**: geusebroek@uva.nl  

**Meta Album author**: Ihsan Ullah  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@article{BURGHOUTS2009306,
    title = {Material-specific adaptation of color invariant features},
    journal = {Pattern Recognition Letters},
    volume = {30},
    number = {3},
    pages = {306-313},
    year = {2009},
    issn = {0167-8655},
    doi = {https://doi.org/10.1016/j.patrec.2008.10.005},
    url = {https://www.sciencedirect.com/science/article/pii/S0167865508003073},
    author = {Gertjan J. Burghouts and Jan-Mark Geusebroek},
    keywords = {Image modeling, Color, Codebook representation, Texture, Textons},
    abstract = {For the modeling of materials, the mapping of image features onto a codebook of feature representatives receives extensive treatment. For reason of their generality and simplicity, filterbank outputs are commonly used as features. The MR8 filterbank of Varma and Zisserman is performing well in a recent evaluation. In this paper, we construct color invariant filter sets from the original MR8 filterbank. We evaluate several color invariant alternatives over more than 250 real-world materials recorded under a variety of imaging conditions including clutter. Our contribution is a material recognition framework that learns automatically for each material specifically the most discriminative filterbank combination and corresponding degree of color invariance. For a large set of materials each with different physical properties, we demonstrate the material-specific filterbank models to be preferred over models with fixed filterbanks.}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Mini]](https://www.openml.org/d/44304)  [[Extended]](https://www.openml.org/d/44337)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44274) of an [OpenML dataset](https://www.openml.org/d/44274). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44274/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44274/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44274/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

